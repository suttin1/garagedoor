import RPi.GPIO as GPIO
from time import sleep
from flask import Flask
import psycopg2
import example_psql as creds

app = Flask(__name__)

def activate_door():
    GPIO.output(11, GPIO.LOW)
    sleep(1.10)
    GPIO.output(11, GPIO.HIGH)
    sleep(1.10)
    GPIO.output(11, GPIO.LOW)

#not implemented yet
def require_appkey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        with open('api.key', 'r') as apikey:
            key=apikey.read().replace('\n', '')
        #if request.args.get('key') and request.args.get('key') == key:
        if request.headers.get('x-api-key') and request.headers.get('x-api-key') == key:
            return view_function(*args, **kwargs)
        else:
            abort(401)
    return decorated_function

def postgress_update(current_stat, action_verb, opposite):
        print("Door was %s, now %s door." % (current_stat, action_verb))
        status_update = "update status SET door_status = '%s';" % opposite
        count_increment = "update cycle_count SET number_of_cycles = number_of_cycles + 1;"
        cur = conn.cursor()
        cur.execute(status_update)
        cur.execute(count_increment)
        conn.commit()

def postgres_flip(current_status):
    if current_status == "closed":        
        postgress_update( "closed", "opening", "open")
        return "Door is opening"
    if current_status == "open":
        postgress_update("open", "closing", "closed")
        return "Door is closing"


def postgress_lookup():
    sql_command = "SELECT * FROM status;"
    cur = conn.cursor()
    cur.execute(sql_command)
    results = cur.fetchall()
    return results

def cycle_lookup():
    sql_command = "SELECT * FROM cycle_count;;"
    cur = conn.cursor()
    cur.execute(sql_command)
    results = cur.fetchall()
    return results 

@app.route('/activate')
def activate():
    activate_door()
    results = postgress_lookup()
    door_status = postgres_flip(results[0][0])
    return door_status

@app.route('/')
def hello():
    door_status = postgress_lookup()
    num_of_cycles = cycle_lookup()
    return "Door is currently %s!</br>Door has opened or closed %s times!" % (door_status[0][0], num_of_cycles[0][0])

if __name__ == '__main__':
    conn_string = "host="+ creds.PGHOST +" port="+ "5432" +" dbname="+ creds.PGDATABASE +" user=" + creds.PGUSER \
    +" password="+ creds.PGPASSWORD
    conn=psycopg2.connect(conn_string)
    print("Connected to " + creds.PGHOST)
    GPIO.setwarnings(False)    # Ignore warning for now
    GPIO.setmode(GPIO.BOARD)   # Ususe physical pin numbering
    GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)   # Set pin 8 to be an output pin and set inictial 
    app.run(host="0.0.0.0", debug=True)
